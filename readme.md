Document Freedom Day Artwork
============================

Here you will find all artwork, source files, and proffesionally printable copies of campaign artwork.

Fonts
-----

For vector and bitmap source files to display and export correctly, you need to have the necessary fonts installed on your system. All fonts used have Free licensing and are not limited by restrictions. Copies of all necessary fonts are within the /fonts directory of this repository.

Apps and file extensions
------------------------

To open files ending with .svg you will need [Inkscape](inkscape.org) for optimum compatibility. For .xcf files you will need [Gimp](http://www.gimp.org/). To open .sla files you will need [Scribus](http://www.scribus.net/canvas/Scribus). All are Free Software.

Professionally printable files
------------------------------

These files have gone through a "pre-printing" process to adapt them to meet the needs of professional printers. The specifications we use are:

- 300 DPI (dots per inch) resolution
- PDF 1.4
- CMYK colour model
- 3mm bleed margin on each side of the document
- Transparency where necessary

Send the pre-print PDF files to your printers if you wish to produce physical copies.

Copyright
---------

Unless otherwise stated, all works are Copyright Free Software Foundation Europe e.V., and in many cases joint copyright. A Creative Commons By-SA license is granted.

Contributor licensing
---------------------

Additions, improvements, and translations are very welcome. All contributions to this repository must be [cc-by-sa 4.0](https://creativecommons.org/licenses/by-sa/4.0/) compatible. If you wish to remain the copyright holder or use any other license, either for your work or any of its component parts (e.g. fonts or photography included in artwork you have made), you must [contact us](mailto:contact@documentfreedom.org) first.